# PLOTS OF COVERAGE AROUND MGMT LOCUS

library(scales)

NORMAL = c("EICE18_889F", "ICCE19_366F_HC","ICCE19_431F")
NEO.A = c("EICE18_889H", "EUCE18_1024H", "FRCE17_701H", "PACE17_433H",
          "PVCE17_1247H", "PVCE17_1402H", "PACE17_421H1")

MGMT.COORD = c(3, 1524727, 1530540)

BIN.SIZE = 4e5


# Retrieve coverage values for each sample
cn.neoA = sapply(NEO.A, function(id) {
    cov = read.table(gzfile(paste0("haemolymph/neoplasiaA/", id, "/", id, ".cov.gz")), sep="\t", header=T, as.is=T)
    cov[cov$chr == MGMT.COORD[1] &
                 cov$start >= MGMT.COORD[2]-BIN.SIZE/2 &
                 cov$end <= MGMT.COORD[3]+BIN.SIZE/2,
        c(2:3, 6)]
}, simplify=F)

cn.normal = sapply(NORMAL, function(id) {
    cov = read.table(gzfile(paste0("normals_2N/normals/", id, "/", id, ".cov.gz")), sep="\t", header=T, as.is=T)
    cov[cov$chr == MGMT.COORD[1] &
            cov$start >= MGMT.COORD[2]-BIN.SIZE/2 &
            cov$end <= MGMT.COORD[3]+BIN.SIZE/2,
        c(2:3, 6)]
}, simplify=F)


# Plot
pdf("CopyNumber_MGMT.pdf", 10, 4.5)
par(mfrow=c(2,1), mar=c(2,2,0,2), oma=c(2,2,2,2), mgp=c(1.5,1,0), tck=-0.03)

# Neoplasia A
plot(0, xlim=c(MGMT.COORD[2]-BIN.SIZE/2.1, MGMT.COORD[3]+BIN.SIZE/2.1), ylim=c(0, 6), las=1,
     xlab="", ylab="", yaxs="i", xaxs="i", xpd=NA, cex.axis=0.9, xaxt="n", yaxt="n")
abline(h=1:5, col="grey90")
rect(xleft=MGMT.COORD[2], xright=MGMT.COORD[3], ybottom=-1, ytop=8, col=alpha("steelblue",0.3), border=NA)
mtext("MGMT", at=mean(MGMT.COORD[2:3]))
for (cn in cn.neoA) {
    segments(x0=cn[,1], x1=cn[,2], y0=cn[,3], col=alpha("#BF1818", 0.9), lwd=1.5)
    segments(x0=cn[-nrow(cn), 2], y0=cn[-nrow(cn), 3], y1=cn[-1, 3], col=alpha("#BF1818", 0.9), lwd=1.5)
}

# Normals
plot(0, xlim=c(MGMT.COORD[2]-BIN.SIZE/2.1, MGMT.COORD[3]+BIN.SIZE/2.1), ylim=c(0, 6), las=1,
     xlab="Chromosome 3 position", ylab="Copy number", yaxs="i", xaxs="i", xpd=NA, cex.axis=0.9, xaxt="n", yaxt="n")
abline(h=1:5, col="grey90")
axis(1, mgp=c(1.8, 0.4, 0))
axis(2, at=seq(0, 6, 1), mgp=c(1.8, 0.5, 0), las=1)
rect(xleft=MGMT.COORD[2], xright=MGMT.COORD[3], ybottom=-1, ytop=8, col=alpha("steelblue",0.3), border=NA)
for (cn in cn.normal) {
    segments(x0=cn[,1], x1=cn[,2], y0=cn[,3], col=alpha("grey20", 0.9), lwd=1.5)
    segments(x0=cn[-nrow(cn), 2], y0=cn[-nrow(cn), 3], y1=cn[-1, 3], col=alpha("grey20", 0.9), lwd=1.5)
}

dev.off()

