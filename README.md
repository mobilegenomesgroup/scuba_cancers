This repository contains custom R scripts supporting the analyses presented in the article "The evolution of two transmissible leukaemias colonizing the coasts of Europe" (Bruzos, Santamarina, García-Souto et al., 2023).

This project has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme, Grant agreement No.716290: https://cordis.europa.eu/project/id/716290
